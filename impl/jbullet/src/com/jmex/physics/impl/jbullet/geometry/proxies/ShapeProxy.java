package com.jmex.physics.impl.jbullet.geometry.proxies;

import com.jmex.physics.PhysicsCollisionGeometry;

public interface ShapeProxy {
	public PhysicsCollisionGeometry getJmeShape();
}
